fn main() {
    let args = std::env::args().skip(1);
    let mut listen = false;
    let mut ipport = None;

    for arg in args {
        if arg == "-l" {
            listen = true;
        } else {
            ipport = Some(arg)
        }
    }

    let ipport = ipport.unwrap_or_else(|| {
        eprintln!("\
ERROR: need an `IP:PORT` argument.
Run this program as: tcpchat [-l] <IP:PORT>");
        std::process::exit(1)
    });


    if listen {
        server(&ipport).unwrap_or_else(|e| {
            eprintln!("SERVER ERROR: {} ({})", ipport, e);
            std::process::exit(1)
        });
    } else {
        client(&ipport).unwrap_or_else(|e| {
            eprintln!("CLIENT ERROR: {} ({})", ipport, e);
            std::process::exit(1)
        });
    }
}

fn server(ipport: &str) -> std::io::Result<()> {
    let listener = std::net::TcpListener::bind(ipport)?;
    eprintln!("SERVER: Running on: {}", listener.local_addr()?);
    eprintln!("SERVER: Waiting for new connection");

    let (socket, addr) = listener.accept()?;
    eprintln!("SERVER: Connection from: {}", addr);

    let mut netout = socket.try_clone()?;

    let mut netin = std::io::BufReader::new(socket);

    let stdin = std::io::stdin();
    let mut stdin = stdin.lock();

    let mut stdout = std::io::stdout();

    loop {
        cp("<< ", &mut netin, &mut stdout)?;
        write_flush(&mut stdout, ">> ")?;
        cp("", &mut stdin, &mut netout)?;
    }
}

fn client(ipport: &str) -> std::io::Result<()> {
    let socket = std::net::TcpStream::connect(ipport)?;

    eprintln!("CLIENT: Connected to: {}", socket.peer_addr()?);

    let mut netout = socket.try_clone()?;

    let mut netin = std::io::BufReader::new(socket);

    let stdin = std::io::stdin();
    let mut stdin = stdin.lock();

    let mut stdout = std::io::stdout();

    loop {
        write_flush(&mut stdout, ">> ")?;
        cp("", &mut stdin, &mut netout)?;
        cp("<< ", &mut netin, &mut stdout)?;
    }
}

fn cp<R, W>(prefix: &str, reader: &mut R, writer: &mut W)
            -> std::io::Result<()>
where R: std::io::BufRead,
      W: std::io::Write {
    let mut buff = String::new();
    match reader.read_line(&mut buff) {
        Ok(n) if n == 0 => return eof_error(),
        Err(x) => return Err(x),
        _ => {},
    }
    if buff.is_empty() {
        return eof_error();
    }

    write_flush(writer, &format!("{}{}", prefix, buff))
}

fn write_flush<W>(writer: &mut W, text: &str) -> std::io::Result<()>
where W: std::io::Write {
    write!(writer, "{}", text)?;
    writer.flush()
}

#[derive(Debug)]
struct Eof;

impl std::fmt::Display for Eof {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "EOF")
    }
}

impl std::error::Error for Eof {}

fn eof_error() -> std::io::Result<()> {
    Err(std::io::Error::new(std::io::ErrorKind::Other, Eof))
}
